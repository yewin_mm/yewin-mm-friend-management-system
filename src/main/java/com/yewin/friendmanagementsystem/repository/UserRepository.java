package com.yewin.friendmanagementsystem.repository;

import com.yewin.friendmanagementsystem.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created By Ye Win on 21/04/2020.
 * Description - This class is used for DAO Layer.
 */

public interface UserRepository extends JpaRepository<User,Long> {

    @Query(nativeQuery = true, value = "select * from users where email like :email and is_deleted=false")
    User findByEmail(@Param("email") String email);

}

