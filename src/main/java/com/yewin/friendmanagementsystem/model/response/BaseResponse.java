package com.yewin.friendmanagementsystem.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@ToString
public class BaseResponse {

    private boolean success;
    private List<String> friends;
    private List<String> recipients;
    private List<String> email;
    private Integer count;
    private String message;

}
