package com.yewin.friendmanagementsystem.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRequest {

    private String name;
    private List<String> friends;
    private String email;
    private String requestor;
    private String target;
    private String sender;
    private String text;

}
