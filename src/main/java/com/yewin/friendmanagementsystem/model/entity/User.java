package com.yewin.friendmanagementsystem.model.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "USERS")
@Data
@ToString
public class User {

    @Id
    @Column(name = "id", columnDefinition = "bigserial")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "createdDate")
    Timestamp createdDate;

    @Column(name = "email")
    String email;

    @Column(name = "friendList")
    String friendList;

    @Column(name = "requestList")
    String requestList;

    @Column(name = "confirmList")
    String confirmList;

    @Column(name = "blockList")
    String blockList;

    @Column(name = "blockedByList")
    String blockedByList;

    @Column(name = "isDeleted")
    Boolean isDeleted;

}
