package com.yewin.friendmanagementsystem.utlity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Pattern;

/**
 * Created By Ye Win on 21/04/2020.
 * Description - This Class is used for Validation process.
 */

public class Utility {

    private static final Logger logger = LogManager.getLogger(Utility.class);

    public static boolean validateString(String input){
        boolean b = true;
        if(input==null || input.trim().equals("")){
            b = false;
        }
        return b;
    }

    public static boolean isValidEmail(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null || email.trim().equals(""))
            return false;
        return pat.matcher(email).matches();
    }

    /** Remove if there have email in input list */
    public static String removeEmailinList(String emailList, String targetEmail){
        //retrieve email list from comma separated values by removing comma
        String[] emList = emailList.split("\\s*,\\s*");

        String newEmList="";
        //retrieve single email from email list
        for (String str : emList) {
            //check already request or not and remove if equal already request.
            if(!str.equals(targetEmail)){
                //check for first time adding data or not.
                if(Utility.validateString(newEmList))
                    newEmList = newEmList+","+str;
                else
                    newEmList=str;

            }
        }

        return newEmList;
    }
}
