package com.yewin.friendmanagementsystem.controller;


import com.yewin.friendmanagementsystem.model.request.BaseRequest;
import com.yewin.friendmanagementsystem.model.response.BaseResponse;
import com.yewin.friendmanagementsystem.service.UserService;
import com.yewin.friendmanagementsystem.utlity.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By Ye Win on 21/04/2020.
 * Description - This Class is used for Entry Layer.
 */

@RestController
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    @Qualifier("userService")
    UserService userService;

    @GetMapping("/greeting")
    public String hello(){
        return "hello";
    }

    @PostMapping("/signup")
    public ResponseEntity<BaseResponse> save(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Sign up request - {}", baseRequest);
            baseResponse = userService.save(baseRequest);
            logger.info("Sign up response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/addFriendConnection")
    public ResponseEntity<BaseResponse> addFriendConn(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Add friend connection request - {}", baseRequest);
            baseResponse = userService.addFriendConnection(baseRequest);
            logger.info("Add friend connection response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/retrieveFriends")
    public ResponseEntity<BaseResponse> retrieveFriends(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Retrieve friend connection request - {}", baseRequest);
            baseResponse = userService.retrieveFriendList(baseRequest);
            logger.info("Retrieve friend connection response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/retrieveCommonFriends")
    public ResponseEntity<BaseResponse> retrieveCommonFriendList(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Retrieve friend connection request - {}", baseRequest);
            baseResponse = userService.retrieveCommonFriendList(baseRequest);
            logger.info("Retrieve friend connection response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/requestFriend")
    public ResponseEntity<BaseResponse> requestFriend(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Request friend request - {}", baseRequest);
            baseResponse = userService.requestFriend(baseRequest);
            logger.info("Request friend response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/blockFriend")
    public ResponseEntity<BaseResponse> blockFriend(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("Block friend request - {}", baseRequest);
            baseResponse = userService.blockFriend(baseRequest);
            logger.info("Block friend response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/retrieveAllEmail")
    public ResponseEntity<BaseResponse> retrieveAll(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("retrieveAll request - {}", baseRequest);
            baseResponse = userService.retrieveAll(baseRequest);
            logger.info("retrieveAll response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/unBlockFriend")
    public ResponseEntity<BaseResponse> unBlockFriend(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("UnBlock friend request - {}", baseRequest);
            baseResponse = userService.unBlockFriend(baseRequest);
            logger.info("UnBlock friend response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/unFriend")
    public ResponseEntity<BaseResponse> unFriend(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("unFriend request - {}", baseRequest);
            baseResponse = userService.unFriend(baseRequest);
            logger.info("unFriend response - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/deleteAccount")
    public ResponseEntity<BaseResponse> deleteAccount(@RequestBody BaseRequest baseRequest){

        try {
            BaseResponse baseResponse = new BaseResponse();
            logger.info("delete account request - {}", baseRequest);
            baseResponse = userService.deleteAccount(baseRequest);
            logger.info("delete account - {}", baseResponse);
            if(baseResponse.isSuccess()){
                return new ResponseEntity<>(baseResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(baseResponse, HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Internal Server Error. Please contact to Administrator.");
            return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
