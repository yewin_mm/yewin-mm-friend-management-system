package com.yewin.friendmanagementsystem.service;

import com.yewin.friendmanagementsystem.model.request.BaseRequest;
import com.yewin.friendmanagementsystem.model.response.BaseResponse;

public interface UserService {

    BaseResponse save(BaseRequest baseRequest);
    BaseResponse addFriendConnection(BaseRequest baseRequest);
    BaseResponse retrieveFriendList(BaseRequest baseRequest);
    BaseResponse retrieveCommonFriendList(BaseRequest baseRequest);
    BaseResponse requestFriend(BaseRequest baseRequest);
    BaseResponse blockFriend(BaseRequest baseRequest);
    BaseResponse retrieveAll(BaseRequest baseRequest);
    BaseResponse booleanSearch(BaseRequest baseRequest);
    BaseResponse unBlockFriend(BaseRequest baseRequest);
    BaseResponse unFriend(BaseRequest baseRequest);
    BaseResponse deleteAccount(BaseRequest baseRequest);

}
