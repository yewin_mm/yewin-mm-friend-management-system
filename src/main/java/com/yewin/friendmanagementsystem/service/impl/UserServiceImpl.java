package com.yewin.friendmanagementsystem.service.impl;

import com.yewin.friendmanagementsystem.model.entity.User;
import com.yewin.friendmanagementsystem.model.request.BaseRequest;
import com.yewin.friendmanagementsystem.model.response.BaseResponse;
import com.yewin.friendmanagementsystem.repository.UserRepository;
import com.yewin.friendmanagementsystem.service.UserService;
import com.yewin.friendmanagementsystem.utlity.Utility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By Ye Win on 21/04/2020.
 * Description - This class is used for Business Logic Layer.
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public synchronized BaseResponse save(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();

        //check incoming data is null, empty or not
        if (Utility.validateString(baseRequest.getName()) && Utility.validateString(baseRequest.getEmail())) {

            //check email format is valid or not.
            if(!Utility.isValidEmail(baseRequest.getEmail())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not correct!");
                logger.warn("Email format is not correct, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //find email is already exist or not. if not existed sign up process is ok.
            User user1 = userRepository.findByEmail(baseRequest.getEmail());
            if (user1!=null) {
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Email is already existed!");
                    return baseResponse;
            }
            User user = new User();
            user.setName(baseRequest.getName());
            user.setEmail(baseRequest.getEmail());
            user.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            user.setFriendList("");
            user.setConfirmList("");
            user.setRequestList("");
            user.setBlockList("");
            user.setBlockedByList("");
            user.setIsDeleted(false);
            userRepository.save(user);
            logger.info("successfully saved!");
            baseResponse.setSuccess(true);
            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public synchronized BaseResponse addFriendConnection(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        int count=0;
        User user1 = new User();
        User user2 = new User();
        int id1,id2;

        //check incoming data is null or not
        if(baseRequest.getFriends()==null){
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }

        //check email list is equal 2 or not
        if(baseRequest.getFriends().size()==2){
            for(String email : baseRequest.getFriends()){

                //check email format is valid or not
                if(!Utility.isValidEmail(email)) {
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Email format is not valid!");
                    logger.warn("Email format is not valid, email - {}", email);
                    return baseResponse;
                }

                //find email is already exist or not. if not existed, need to sign up first.
                User user = userRepository.findByEmail(email);

                //check email is register or not.
                if(user==null){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Email is not register, please sign up first!");
                    logger.warn("Email is not register, please sign up first, email - {}", email);
                    return baseResponse;
                }
                count = count +1;
                if(count==1)
                    user1 = user;
                else
                    user2 = user;
            }

            //retrieve block list from comma separated values by removing comma
            String[] blockList = user1.getBlockList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockList) {
                if(str.equals(user2.getEmail())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't add friend connection, because you blocked that user! Please unblock first.");
                    logger.warn("Can't add friend connection, because you blocked that user! Please unblock first. email - {}", user2.getEmail());
                    return baseResponse;
                }
            }

            //retrieve block by list from comma separated values by removing comma
            String[] blockByList = user1.getBlockedByList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockByList) {
                if(str.equals(user2.getEmail())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't add friend connection, because you have been blocked by that user!");
                    logger.warn("Can't add friend connection, because you have been blocked by that user! email - {}", user2.getEmail());
                    return baseResponse;
                }
            }

            //retrieve email list from comma separated values by removing comma
            String[] friList = user1.getFriendList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : friList) {
                //check duplicate adding friend connection
                if(str.equals(user2.getEmail())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Already Friend!");
                    logger.warn("Already Friend email - {}", user2.getEmail());
                    return baseResponse;
                }
            }

            //remove email in confirm list for user1.
            String newUser1ConfirmList = Utility.removeEmailinList(user1.getConfirmList(),user2.getEmail());

            //remove email in request list for user1.
            String newUser1ReqList = Utility.removeEmailinList(user1.getRequestList(),user2.getEmail());

            //remove email in block list for user1.
            String newUser1BlockList = Utility.removeEmailinList(user1.getBlockList(),user2.getEmail());

            //remove email in blocked by list for user1.
            String newUser1BlockByList = Utility.removeEmailinList(user1.getBlockedByList(),user2.getEmail());

            //update for updated requestList and confirmlist in user1
            user1.setRequestList(newUser1ReqList);
            user1.setConfirmList(newUser1ConfirmList);
            user1.setBlockList(newUser1BlockList);
            user1.setBlockedByList(newUser1BlockByList);

            //check first time adding or not
            if(user1.getFriendList().equals(""))
                user1.setFriendList(user2.getEmail());
            else user1.setFriendList(user1.getFriendList()+","+user2.getEmail());
            userRepository.save(user1);

            //remove email in confirm list for user2.
            String newUser2ConfirmList = Utility.removeEmailinList(user2.getConfirmList(),user1.getEmail());

            //remove email in request list for user2.
            String newUser2ReqList = Utility.removeEmailinList(user2.getRequestList(),user1.getEmail());

            //remove email in block list for user2.
            String newUser2BlockList = Utility.removeEmailinList(user2.getBlockList(),user1.getEmail());

            //remove email in blocked by list for user2.
            String newUser2BlockByList = Utility.removeEmailinList(user2.getBlockedByList(),user1.getEmail());

            //update for updated requestList and confirmlist in user2
            user2.setRequestList(newUser2ReqList);
            user2.setConfirmList(newUser2ConfirmList);
            user2.setBlockList(newUser2BlockList);
            user2.setBlockedByList(newUser2BlockByList);

            //check first time adding or not
            if(user2.getFriendList().equals(""))
                user2.setFriendList(user1.getEmail());
            else user2.setFriendList(user2.getFriendList()+","+user1.getEmail());
            userRepository.save(user2);

            logger.info("successfully updated friend list!");
            baseResponse.setSuccess(true);
            return baseResponse;

        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Please add two email address!");
            logger.warn("Please add two email address, size - {}",baseRequest.getFriends().size());
            return baseResponse;
        }
    }

    @Override
    public BaseResponse retrieveFriendList(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();

        if(Utility.validateString(baseRequest.getEmail())) {

            if (!Utility.isValidEmail(baseRequest.getEmail())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //find email is already exist or not. if not existed, need to sign up first.
            User user = userRepository.findByEmail(baseRequest.getEmail());

            //check email is register or not.
            if(user==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //retrieve email list from comma separated values by removing comma
            String[] friList = user.getFriendList().split("\\s*,\\s*");
            List<String> friendList = new ArrayList<>();

            //convert to List<String> type from String[] array type
            for (String str : friList) {
                friendList.add(str);
            }
            baseResponse.setSuccess(true);
            baseResponse.setFriends(friendList);
            baseResponse.setCount(friendList.size());
            if(friendList.size()==0){
                baseResponse.setMessage("No friend was found");
            }
            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse retrieveCommonFriendList(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        int count=0;
        User user1 = new User();
        User user2 = new User();

        if(baseRequest.getFriends()==null){
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }

        //check email list is equal 2 or not
        if(baseRequest.getFriends().size()==2) {
            for (String email : baseRequest.getFriends()) {
                if (!Utility.isValidEmail(email)) {
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Email format is not valid!");
                    logger.warn("Email format is not valid, email - {}", email);
                    return baseResponse;
                }

                //find email is already exist or not. if not existed, need to sign up first.
                User user = userRepository.findByEmail(email);
                if (user == null) {
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Email is not register, please sign up first!");
                    logger.warn("Email is not register, please sign up first, email - {}", email);
                    return baseResponse;
                }
                //mark to count first time loop or second time loop.
                count = count +1;

                //for first time loop.
                if(count==1)
                    user1 = user;
                //for second time loop.
                else
                    user2 = user;
            }

            //retrieve email list from comma separated values by removing comma
            String[] usr1friList = user1.getFriendList().split("\\s*,\\s*");
            List<String> usr1friendList = new ArrayList<>();

            //convert to List<String> type from String[] array type
            for (String str : usr1friList) {
                //adding email to response list but remove user2 email.
                if(!str.equals(user2.getEmail())){
                    usr1friendList.add(str);
                }
            }

            String[] usr2friList = user2.getFriendList().split("\\s*,\\s*");
            List<String> usr2friendList = new ArrayList<>();

            //convert to List<String> type from String[] array type
            for (String str : usr2friList) {
                //adding email to response list but remove user1 email.
                if(!str.equals(user1.getEmail())){
                    usr2friendList.add(str);
                }
            }

            List<String> friendList = new ArrayList<>();

            //check user1 list data contain user2 list data
            for(String user1Friend : usr1friendList){
                if(usr2friendList.contains(user1Friend)){
                    //adding common contain data to response list.
                    friendList.add(user1Friend);
                }
            }

            baseResponse.setSuccess(true);
            baseResponse.setFriends(friendList);
            baseResponse.setCount(friendList.size());
            if(friendList.size()==0){
                baseResponse.setMessage("No mutual friend was found");
            }
            return baseResponse;

        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Please add two email address!");
            logger.warn("Please add two email address, size - {}",baseRequest.getFriends().size());
            return baseResponse;
        }
    }

    @Override
    public synchronized BaseResponse requestFriend(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        if (Utility.validateString(baseRequest.getRequestor()) && Utility.validateString(baseRequest.getTarget())) {

            if(!Utility.isValidEmail(baseRequest.getRequestor())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }
            if(!Utility.isValidEmail(baseRequest.getTarget())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //find request email is already exist or not. if not existed, need to sign up first.
            User reqUser = userRepository.findByEmail(baseRequest.getRequestor());
            if(reqUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }

            //retrieve blocked by list from comma separated values by removing comma
            String[] blockByList = reqUser.getBlockedByList().split("\\s*,\\s*");
            for(String str : blockByList){
                //check already friend or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't add, because you have been blocked by that user!");
                    logger.warn("Can't add, because you have been blocked by that user!, email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve block list from comma separated values by removing comma
            String[] blockList = reqUser.getBlockList().split("\\s*,\\s*");
            for(String str : blockList){
                //check already friend or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't add, because you blocked that user! Please unblock first.");
                    logger.warn("Can't add, because you blocked that user! Please unblock first. email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve friend list from comma separated values by removing comma
            String[] friList = reqUser.getFriendList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : friList) {
                //check already friend or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Already Friend!");
                    logger.warn("Already Friend, email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve request list from comma separated values by removing comma
            String[] reqList = reqUser.getRequestList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : reqList) {
                //check duplicate adding request connection
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Already Requested!");
                    logger.warn("Already Requested, email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve confirm list from comma separated values by removing comma
            String[] confirmList = reqUser.getConfirmList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : confirmList) {
                //check requested user is already in confirm list.
                if(str.equals(baseRequest.getTarget())){
                    List<String> friendLists = new ArrayList<>();
                    friendLists.add(baseRequest.getRequestor());
                    friendLists.add(baseRequest.getTarget());
                    baseRequest.setFriends(friendLists);

                    //making friend these two emails because requested email is already in confirm list by calling existed addfriendconnection in above.
                    baseResponse = addFriendConnection(baseRequest);
                    return baseResponse;
                }
            }

            //find target email is already exist or not. if not existed, need to sign up first.
            User targetUser = userRepository.findByEmail(baseRequest.getTarget());
            if(targetUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //check first time adding or not
            if(reqUser.getRequestList().equals(""))
                reqUser.setRequestList(targetUser.getEmail());
            else reqUser.setRequestList(reqUser.getRequestList()+","+targetUser.getEmail());
            userRepository.save(reqUser);

            //check first time adding or not
            if(targetUser.getConfirmList().equals(""))
                targetUser.setConfirmList(reqUser.getEmail());
            else targetUser.setConfirmList(targetUser.getConfirmList()+","+reqUser.getEmail());
            userRepository.save(targetUser);

            logger.info("successfully updated request list and confirm friend list!");
            baseResponse.setSuccess(true);
            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public synchronized BaseResponse blockFriend(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        if (Utility.validateString(baseRequest.getRequestor()) && Utility.validateString(baseRequest.getTarget())) {

            if(!Utility.isValidEmail(baseRequest.getRequestor())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }
            if(!Utility.isValidEmail(baseRequest.getTarget())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //find request email is already exist or not. if not existed, need to sign up first.
            User reqUser = userRepository.findByEmail(baseRequest.getRequestor());
            if(reqUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }

            //retrieve block list from comma separated values by removing comma
            String[] blockList = reqUser.getBlockList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockList) {
                //check already block or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Already Blocked!");
                    logger.warn("Already Blocked, email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve blockby list from comma separated values by removing comma
            String[] blockByList = reqUser.getBlockedByList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockByList) {
                //check already blocked by or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't Block, because you have been blocked by that user!");
                    logger.warn("Can't Block, because you have been blocked by that user!, email - {}", baseRequest.getRequestor());
                    return baseResponse;
                }
            }

            //find target email is already exist or not. if not existed, need to sign up first.
            User targetUser = userRepository.findByEmail(baseRequest.getTarget());
            if(targetUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //remove email in friend list for req user.
            String newReqUserFriList = Utility.removeEmailinList(reqUser.getFriendList(),baseRequest.getTarget());

            //remove email in request list for req user.
            String newReqUserReqList = Utility.removeEmailinList(reqUser.getRequestList(),baseRequest.getTarget());

            //remove email in confirm list for req user.
            String newReqUserConfirmList = Utility.removeEmailinList(reqUser.getConfirmList(),baseRequest.getTarget());

            //update for updated friend list, requestList and confirmlist in requestUser
            reqUser.setFriendList(newReqUserFriList);
            reqUser.setRequestList(newReqUserReqList);
            reqUser.setConfirmList(newReqUserConfirmList);

            //remove email in friend list for target user.
            String newTargetUserFriList = Utility.removeEmailinList(targetUser.getFriendList(),baseRequest.getRequestor());

            //remove email in request list for target user.
            String newTargetUserReqList = Utility.removeEmailinList(targetUser.getRequestList(),baseRequest.getRequestor());

            //remove email in confirm list for target user.
            String newTargetUserConfirmList = Utility.removeEmailinList(targetUser.getConfirmList(),baseRequest.getRequestor());

            //update for updated friend list, requestList and confirmlist in targetuser
            targetUser.setFriendList(newTargetUserFriList);
            targetUser.setRequestList(newTargetUserReqList);
            targetUser.setConfirmList(newTargetUserConfirmList);

            //check first time adding or not in blocklist
            if(reqUser.getBlockList().equals(""))
                reqUser.setBlockList(targetUser.getEmail());
            else reqUser.setBlockList(reqUser.getBlockList()+","+targetUser.getEmail());
            userRepository.save(reqUser);

            //check first time adding or not  in blockbylist
            if(targetUser.getBlockedByList().equals(""))
                targetUser.setBlockedByList(reqUser.getEmail());
            else targetUser.setBlockedByList(targetUser.getBlockedByList()+","+reqUser.getEmail());
            userRepository.save(targetUser);

            logger.info("successfully blocked from user - {} to user - {}",baseRequest.getRequestor(),baseRequest.getTarget());
            baseResponse.setSuccess(true);
            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse retrieveAll(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();

        if (Utility.validateString(baseRequest.getSender()) && Utility.validateString(baseRequest.getText())) {

            if (!Utility.isValidEmail(baseRequest.getSender())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //find email is already exist or not. if not existed, need to sign up first.
            User user = userRepository.findByEmail(baseRequest.getSender());

            //check email is register or not.
            if(user==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            List<String> recipientList = new ArrayList<>();

            //retrieve friend email list from comma separated values by removing comma
            String[] friList = user.getFriendList().split("\\s*,\\s*");

            //retrieve request email list from comma separated values by removing comma
            String[] reqList = user.getRequestList().split("\\s*,\\s*");

            //retrieve confirm email list from comma separated values by removing comma
            String[] conList = user.getConfirmList().split("\\s*,\\s*");

            //convert to List<String> type from String[] array type
            for (String str : friList) {
                //check empty string. if empty, it will not add in array list and if not, it will add in list.
                if(Utility.validateString(str))
                    recipientList.add(str);
            }
            for (String str : reqList) {
                //check empty string. if empty, it will not add in array list and if not, it will add in list.
                if(Utility.validateString(str))
                    recipientList.add(str);
            }
            for (String str : conList) {
                //check empty string. if empty, it will not add in array list and if not, it will add in list.
                if(Utility.validateString(str))
                    recipientList.add(str);
            }

            baseResponse.setSuccess(true);
            baseResponse.setRecipients(recipientList);
            baseResponse.setCount(recipientList.size());
            if(recipientList.size()==0){
                baseResponse.setMessage("No friend or confirm friend or request friend was found");
            }
            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse booleanSearch(BaseRequest baseRequest) {
        return null;
    }

    @Override
    public BaseResponse unBlockFriend(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        if (Utility.validateString(baseRequest.getRequestor()) && Utility.validateString(baseRequest.getTarget())) {

            if(!Utility.isValidEmail(baseRequest.getRequestor())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }
            if(!Utility.isValidEmail(baseRequest.getTarget())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //find request email is already exist or not. if not existed, need to sign up first.
            User reqUser = userRepository.findByEmail(baseRequest.getRequestor());
            if(reqUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }

            //retrieve blockby list from comma separated values by removing comma
            String[] blockByList = reqUser.getBlockedByList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockByList) {
                //check already blocked by or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't Unblock, because you have been blocked by that user!");
                    logger.warn("Can't Unblock, because you have been blocked by that user!, email - {}", baseRequest.getRequestor());
                    return baseResponse;
                }
            }

            //retrieve block list from comma separated values by removing comma
            String[] blockList = reqUser.getBlockList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockList) {
                //check already block or not
                if(str.equals(baseRequest.getTarget())){
                    //find target email is already exist or not. if not existed, need to sign up first.
                    User targetUser = userRepository.findByEmail(baseRequest.getTarget());
                    if(targetUser==null){
                        baseResponse.setSuccess(false);
                        baseResponse.setMessage("Email is not register, please sign up first!");
                        logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getTarget());
                        return baseResponse;
                    }

                    //remove email in block list for request user.
                    String newReqUserBlockList = Utility.removeEmailinList(reqUser.getBlockList(),baseRequest.getTarget());

                    //update for updated block list in requestUser
                    reqUser.setBlockList(newReqUserBlockList);

                    //remove email in block by list for target user.
                    String newTargetUserBlockedByList = Utility.removeEmailinList(targetUser.getBlockedByList(),baseRequest.getRequestor());

                    //update for updated blocked by list in target user
                    targetUser.setBlockedByList(newTargetUserBlockedByList);

                    userRepository.save(reqUser);

                    userRepository.save(targetUser);

                    logger.info("successfully unblocked from user - {} to user - {}",baseRequest.getRequestor(),baseRequest.getTarget());
                    baseResponse.setSuccess(true);
                    return baseResponse;
                }
            }

            baseResponse.setSuccess(false);
            baseResponse.setMessage("Can't unblock, because you didn't block that user!");
            logger.warn("Can't unblock, because you didn't block that user!, email - {}", baseRequest.getTarget());
            return baseResponse;

        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse unFriend(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();
        if (Utility.validateString(baseRequest.getRequestor()) && Utility.validateString(baseRequest.getTarget())) {

            if(!Utility.isValidEmail(baseRequest.getRequestor())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }
            if(!Utility.isValidEmail(baseRequest.getTarget())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getTarget());
                return baseResponse;
            }

            //find request email is already exist or not. if not existed, need to sign up first.
            User reqUser = userRepository.findByEmail(baseRequest.getRequestor());
            if(reqUser==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getRequestor());
                return baseResponse;
            }

            //retrieve block list from comma separated values by removing comma
            String[] blockList = reqUser.getBlockList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockList) {
                //check already block or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't unfriend, because you blocked that user!");
                    logger.warn("Can't unfriend, because you blocked that user!, email - {}", baseRequest.getTarget());
                    return baseResponse;
                }
            }

            //retrieve blockby list from comma separated values by removing comma
            String[] blockByList = reqUser.getBlockedByList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : blockByList) {
                //check already blocked by or not
                if(str.equals(baseRequest.getTarget())){
                    baseResponse.setSuccess(false);
                    baseResponse.setMessage("Can't unfriend, because you have been blocked by that user!");
                    logger.warn("Can't unfriend, because you have been blocked by that user!, email - {}", baseRequest.getRequestor());
                    return baseResponse;
                }
            }

            //retrieve friend list from comma separated values by removing comma
            String[] friList = reqUser.getFriendList().split("\\s*,\\s*");
            //retrieve single email from email list
            for (String str : friList) {
                //check already block or not
                if(str.equals(baseRequest.getTarget())){
                    //find target email is already exist or not. if not existed, need to sign up first.
                    User targetUser = userRepository.findByEmail(baseRequest.getTarget());
                    if(targetUser==null){
                        baseResponse.setSuccess(false);
                        baseResponse.setMessage("Email is not register, please sign up first!");
                        logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getTarget());
                        return baseResponse;
                    }

                    //remove email in friend list for request user.
                    String newReqUserFriList = Utility.removeEmailinList(reqUser.getFriendList(),baseRequest.getTarget());

                    //update for updated friend list in requestUser
                    reqUser.setFriendList(newReqUserFriList);

                    //remove email in friend list for target user.
                    String newTargetUserFriList = Utility.removeEmailinList(targetUser.getFriendList(),baseRequest.getRequestor());

                    //update for updated friend list in target user
                    targetUser.setFriendList(newTargetUserFriList);

                    userRepository.save(reqUser);

                    userRepository.save(targetUser);

                    logger.info("successfully un friend from user - {} to user - {}",baseRequest.getRequestor(),baseRequest.getTarget());
                    baseResponse.setSuccess(true);
                    return baseResponse;
                }
            }

            baseResponse.setSuccess(false);
            baseResponse.setMessage("Can't unfriend, because you are not friend with that user!");
            logger.warn("Can't unfriend, because you are not friend with that user!, email - {}", baseRequest.getTarget());
            return baseResponse;

        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

    @Override
    public BaseResponse deleteAccount(BaseRequest baseRequest) {
        BaseResponse baseResponse = new BaseResponse();

        if(Utility.validateString(baseRequest.getEmail())) {

            if (!Utility.isValidEmail(baseRequest.getEmail())) {
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email format is not valid!");
                logger.warn("Email format is not valid, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //find email is already exist or not. if not existed, need to sign up first.
            User user = userRepository.findByEmail(baseRequest.getEmail());

            //check email is register or not.
            if(user==null){
                baseResponse.setSuccess(false);
                baseResponse.setMessage("Email is not register, please sign up first!");
                logger.warn("Email is not register, please sign up first, email - {}", baseRequest.getEmail());
                return baseResponse;
            }

            //delete account
            user.setIsDeleted(true);

            //update table
            userRepository.save(user);

            baseResponse.setSuccess(true);

            return baseResponse;
        }else {
            baseResponse.setSuccess(false);
            baseResponse.setMessage("Actual Required Data is null or empty!");
            logger.warn("Actual Required Data is null or empty");
            return baseResponse;
        }
    }

}
